//
//  Product.swift
//  Liverpool
//
//  Created by Angel  Solsona on 14/02/18.
//  Copyright © 2018 Solsona. All rights reserved.
//

import UIKit
import SwiftyJSON;
class Product: NSObject {

    var title:String = "";
    var price:String = "";
    var urlImage:String = "";
    
    init(json:JSON) {
        let attributes = json["attributes"];
        print(attributes["product.displayName"][0].string!)
        guard
            let url = attributes["sku.smallImage"][0].string,
            let price = attributes["sku.sale_Price"][0].string,
            let title = attributes["product.displayName"][0].string
        else{
            return
        }

        self.title=title;
        self.price=price;
        self.urlImage=url;
        
    }
    

}
