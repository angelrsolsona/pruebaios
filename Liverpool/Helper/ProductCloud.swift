//
//  ProductCloud.swift
//  Liverpool
//
//  Created by Angel  Solsona on 14/02/18.
//  Copyright © 2018 Solsona. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ProductCloud: NSObject {

    class func getProductSearch(parameters:[String:Any]?, completationHandler:@escaping(_ result:JSON, _ success:Bool, _ message:String, _ list:[Product])->()){
        if let param = parameters{
            let search = param["search"] as! String
            let url = "https://www.liverpool.com.mx/tienda/?s=\(search)&d3106047a194921c01969dfdec083925=json";
            Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.queryString).responseJSON { (response) in
                
                switch(response.result){
                case .success:
                    if response.response?.statusCode != nil{
                        if let dictResponse = response.result.value{
                            let json = JSON(dictResponse)
                            var records = json["contents"][0]["mainContent"][3]["contents"][0]["records"]
                            var arrayProducts:[Product] = [Product]()
                            for (_,subJSON):(String,JSON) in records{
                                let product = Product(json: subJSON)
                                arrayProducts.append(product);
                            }
                            print(arrayProducts.description);
                            if arrayProducts.count>0{
                                completationHandler(json, true, "Se encontraron resultados", arrayProducts);
                            }else{
                                completationHandler(json, false, "No se encontraron resultados", arrayProducts);
                            }
                            
                            
                            
                        }
                    }
                    break;
                case .failure(let error):
                    completationHandler(JSON.null,false,error.localizedDescription,[]);
                    break;
                }
                
            }
        }
        
    }
    
}
