//
//  Extensions.swift
//  Liverpool
//
//  Created by Angel  Solsona on 14/02/18.
//  Copyright © 2018 Solsona. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController{
    
    func alertNotice(view:UIViewController, message:String){
        
        let alert = UIAlertController(title: "Aviso",
                                      message: message,
                                      preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        view.present(alert, animated: true, completion: nil)
        
    }
}

extension UIImageView{
    
    func imageFromURL(urlString:String){
        if let url = URL(string: urlString){
            let request = URLRequest(url: url)
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, urlRequest, error) in
                if let imageData = data{
                    DispatchQueue.main.async {
                        self.image = UIImage(data: imageData)
                    }
                    
                }
            })
            task.resume()
        }
    }
}

extension UISearchBar {
    public func setSerchTextcolor(color: UIColor) {
        let clrChange = subviews.flatMap { $0.subviews }
        guard let sc = (clrChange.filter { $0 is UITextField }).first as? UITextField else { return }
        sc.textColor = color
    }
}





