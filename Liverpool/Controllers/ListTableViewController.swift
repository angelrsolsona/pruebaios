//
//  ListTableViewController.swift
//  Liverpool
//
//  Created by Angel  Solsona on 14/02/18.
//  Copyright © 2018 Solsona. All rights reserved.
//

import UIKit
import APESuperHUD
class ListTableViewController: UITableViewController {

    var arrayProducts:[Product] = []
    var historySearch:[String] = []
    let searchController = UISearchController(searchResultsController: nil);
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.tintColor = UIColor.white
        searchController.searchBar.autocapitalizationType = .none
        searchController.searchBar.setSerchTextcolor(color: UIColor.white)
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        getHistory()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if isFiltering(){
            return historySearch.count
        }
        return arrayProducts.count;
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isFiltering(){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellHistory", for: indexPath) as! HistoryTableViewCell
            let history = self.historySearch[indexPath.row]
            cell.title.text = history
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ProductTableViewCell
            
            // Configure the cell...
            let product = self.arrayProducts[indexPath.row]
            cell.imageProduct.imageFromURL(urlString: product.urlImage)
            cell.title.text = product.title
            cell.price.text = "$\(product.price)"
            return cell
        }
        

        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isFiltering(){
            return 50
        }
        return 128
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        
        self.tableView.reloadData()
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func saveHistory(text:String){
        print(text)
        var stringSave:String = ""
        if let history = UserDefaults.standard.object(forKey: "history"){
            let historyString = history as! String
            stringSave="\(historyString)&&\(text)"
        }else{
            stringSave = text
        }
        UserDefaults.standard.set(stringSave, forKey:"history")
        UserDefaults.standard.synchronize()
        getHistory()
    }
    func getHistory(){
        var arrayHistory:[String]=[]
        if let history = UserDefaults.standard.object(forKey: "history"){
            let historyString = history as! String
            arrayHistory = historyString.components(separatedBy:"&&")
        }else{
            arrayHistory = []
        }
        self.historySearch = arrayHistory
    }
    
    func searchProduct(text:String){
        searchController.isActive=false
        let params = ["search":text]
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Buscando Producto", presentingView: (self.navigationController?.view!)!)
        ProductCloud.getProductSearch(parameters: params) { (json, success, message, listProduct) in
            APESuperHUD.removeHUD(animated: true, presentingView: (self.navigationController?.view)!, completion: {
                DispatchQueue.main.async {
                    if success{
                        self.arrayProducts = listProduct
                        self.tableView.reloadData()
                    }else{
                        let alert = UIAlertController()
                        alert.alertNotice(view: self, message: message)
                    }
                }
            })
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ListTableViewController:UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
}

extension ListTableViewController:UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let text = searchBar.text!
        
        searchProduct(text: text)
        saveHistory(text: text)
    }
}
