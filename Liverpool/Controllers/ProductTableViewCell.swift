//
//  ProductTableViewCell.swift
//  Liverpool
//
//  Created by Angel  Solsona on 14/02/18.
//  Copyright © 2018 Solsona. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        imageProduct.image = nil
        title.text = ""
        price.text = ""
    }

}
