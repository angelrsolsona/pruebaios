//
//  HistoryTableViewCell.swift
//  Liverpool
//
//  Created by Angel  Solsona on 14/02/18.
//  Copyright © 2018 Solsona. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        title.text = ""
    }

}
